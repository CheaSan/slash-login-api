const mongoose = require("mongoose");

const Schema = new mongoose.Schema({
  full_name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
});

const Model = mongoose.model("User", Schema);

module.exports = Model;
