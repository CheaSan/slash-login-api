const User = require("../models/User");
bcrypt = require("bcryptjs");
const _ = require("lodash");

module.exports = {
  login,
  register,
};

async function login(req, res) {
  const { username, password } = req.body;

  console.log("===> Hello: ", req.body);
  const user = await User.findOne({
    username,
  });

  if (_.isEmpty(user))
    return res.status(400).send({
      message: "User not found.",
      data: {},
    });

  const isMatch = await bcrypt.compareSync(password, user.password);

  if (isMatch) {
    return res.status(200).send({
      message: "You are logged in.",
      data: user,
    });
  } else {
    return res.status(400).send({
      message: "Log in failed.",
      data: {},
    });
  }
}

async function register(req, res) {
  const { name, email, password } = req.body;

  console.log("===> Body: ", req.body);

  const salt = bcrypt.genSaltSync(10);
  const hashPassword = bcrypt.hashSync(password, salt);

  try {
    const user = new User({
      email,
      full_name: name,
      password: hashPassword,
    });

    await user.save(function (err) {
      if (err) {
        console.log(err);
      }
    });

    return res.status(200).send({ message: "Success.", data: user });
  } catch (error) {
    console.log("===> Something went wrong: ", error);
    return res
      .status(500)
      .send({ message: "Something went wrong.", data: error });
  }
}
